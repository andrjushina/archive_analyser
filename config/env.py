import os
from os.path import join, dirname, exists
from dotenv import load_dotenv
import json

dotenv_path = join(dirname(__file__), '.env' )
load_dotenv(dotenv_path)

VERSION = os.environ.get("VERSION")
LICENSE = os.environ.get("LICENSE")
AUTHOR = os.environ.get("AUTHOR")

PATHS = {"source01": os.environ.get("SOURCE01"),
    "source02": os.environ.get("SOURCE02"),
    "source03": os.environ.get("SOURCE03"),
    "source04": os.environ.get("SOURCE04"),
    "source05": os.environ.get("SOURCE05"),
    "source06": os.environ.get("SOURCE06"),
    "source07": os.environ.get("SOURCE07"),
    "source08": os.environ.get("SOURCE08"),
    "source09": os.environ.get("SOURCE09"),
    "source10": os.environ.get("SOURCE10")
}

def getVersion():
    if exists(dotenv_path) == True:
       return VERSION
    else:
       return False

def getLicense():
    if exists(dotenv_path) == True:
        return LICENSE
    else:
       return False
    
def getAuthor():
    if exists(dotenv_path) == True:
        return AUTHOR
    else:
       return False

def getConfiguredPathsFromDotEnv():
    if (exists(dotenv_path) == False 
        or (PATHS["source01"] is None
              and PATHS["source02"] is None
              and PATHS["source03"] is None
              and PATHS["source04"] is None
              and PATHS["source05"] is None
              and PATHS["source06"] is None
              and PATHS["source07"] is None
              and PATHS["source08"] is None
              and PATHS["source09"] is None
              and PATHS["source10"] is None)):
        return False
    else:
      paths = {}
      for i, k in PATHS.items():
          if len(k) > 0:
              paths[i]=k
      return paths