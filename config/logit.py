import logging
from config import config

def getLogger():

    logging.basicConfig(
        filename= config.getLogFileLocation(),
        level = _setLogLevel(config.getLogConfigForFile()),
        format= '''[%(asctime)s] %(levelname)s: %(message)s 
        - process: %(process)d 
        - process name: %(processName)s 
        - module: %(module)s
        - path: {%(filename)s:%(lineno)d}
        - function: %(funcName)s
        - logger: %(name)s''', 
        datefmt='%H:%M:%S')

    console = logging.StreamHandler()
    console.setLevel(_setLogLevel(config.getLogConfigForConsole()))
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)

    logger = logging.getLogger(__name__)

    return logger

def _setLogLevel(level):
    switch={
      "NOTSET": logging.NOTSET,
      "DEBUG": logging.DEBUG,
      "INFO": logging.INFO,
      "WARNING": logging.WARN,
      "ERROR": logging.ERROR,
      "CRITICAL": logging.CRITICAL
      }
    return switch.get(level,"Unsupported log level")