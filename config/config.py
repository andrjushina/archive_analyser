import json
from os import mkdir
from os.path import join, dirname, exists
from . import env

config_path = join(dirname(__file__), 'config.json' )
log_path = join(dirname(__file__), './logs' )

def getConfig():
    if exists(config_path) == True:
        with open(config_path, 'r') as file:
            data = json.load(file)
            return data
    else:
        print("Unable to locate configuration file\n\
              Please check the state of your ./config folder\n")
        raise Exception("Something went wrong with the project configuration")

def getConfigPrettyPrint():
    if exists(config_path) == True:
        with open(config_path, 'r') as file:
            data = json.load(file)
            return json.dumps(data, indent=4)
    else:
        print("Unable to locate configuration file\n\
              Please check the state of your ./config folder\n")
        raise Exception("Something went wrong with the project configuration")
    
def getLogFileLocation():
      c = getConfig()
      log_folder = c["logger"]["log_folder"]
      if exists(log_folder) == False:
          _createLogPath()
      return c["logger"]["logger_file_location"]
        
def getLogConfigForFile():
    c = getConfig()
    log_folder = c["logger"]["log_folder"]
    if exists(log_folder) == False:
        _createLogPath()
    return c["logger"]["file_logger"]

def getLogConfigForConsole():
    c = getConfig()
    log_folder = c["logger"]["log_folder"]
    if exists(log_folder) == False:
        _createLogPath()
    return c["logger"]["console_logger"]

def _createLogPath():
    c = getConfig()
    log_folder = c["logger"]["log_folder"]
    if exists(log_folder) == False:
        mkdir(log_folder)
        return exists(log_folder)

def getConfiguredExtensions():
    c = getConfig()
    return c["extensions"]

# returns list
def getIncludedExtensions():
    c = getConfig()
    return c["extensions"]["include"]

# returns list    
def getExcludedExtensions():
    c = getConfig()
    return c["extensions"]["exclude"]

# returns list    
def getTinyTagExtensions():
    c = getConfig()
    return c["extensions"]["supported_by_tinytag_library"]

def getConfiguredExtensionsPrettyPrint():
    c = getConfig()
    return json.dumps(c["extensions"], indent=4)

def getArchiveConfig():
    c = getConfig()
    archive_folder = c["archive"]["archive_folder"]
    if exists(archive_folder) == False:
        _createArchivePath()
    return c["archive"]

def getArchiveFilePath():
    c = getConfig()
    archive_folder = c["archive"]["archive_folder"]
    if exists(archive_folder) == False:
        _createArchivePath()
    return c["archive"]["path_to_archive"]

def getArchiveMode():
    c = getConfig()
    mode = c["archive"]["archiving_mode"]
    print("Actual archiving mode is: " + mode)
    if _validateArchiveMode(mode) == True:
        return mode 

def _validateArchiveMode(mode):
    if mode not in ['rewrite', 'append']:
        print("Invalid archiving mode\n\
              Please check the state of your configuration file")
        raise Exception
    return True

def _createArchivePath():
    c = getConfig()
    archive_folder = c["archive"]["archive_folder"]
    if exists(archive_folder) == False:
        mkdir(archive_folder)
        return exists(archive_folder)
    
def getPathsConfig():
    c = getConfig()
    if (env.getConfiguredPathsFromDotEnv() is False
        and _processJsonPaths() is False):
        raise Exception ("Something is wrong with pre-configured paths")
    elif (env.getConfiguredPathsFromDotEnv() is False
        and _processJsonPaths() is not False):
        #print("Processed paths are: " + str(_processJsonPaths()))
        return _processJsonPaths()
    else:
        return env.getConfiguredPathsFromDotEnv()

def getPathsConfigPrettyPrint():
    c = getConfig()
    if env.getConfiguredPathsFromDotEnv() is False:
        return json.dumps(c["paths"], indent=4)
    else:
        return json.dumps(env.getConfiguredPathsFromDotEnv(), indent=4)
    
def _processJsonPaths():
    c = getConfig()
    jsonpaths = c["paths"]
    if (jsonpaths["source01"] == '%PLACEHOLDER%'
              and jsonpaths["source02"] == '%PLACEHOLDER%'
              and jsonpaths["source03"] == '%PLACEHOLDER%'
              and jsonpaths["source04"] == '%PLACEHOLDER%'
              and jsonpaths["source05"] == '%PLACEHOLDER%'
              and jsonpaths["source06"] == '%PLACEHOLDER%'
              and jsonpaths["source07"] == '%PLACEHOLDER%'
              and jsonpaths["source08"] == '%PLACEHOLDER%'
              and jsonpaths["source09"] == '%PLACEHOLDER%'
              and jsonpaths["source10"] == '%PLACEHOLDER%'):
        return False
    else:
      filteredpaths = {}
      for i, k in jsonpaths.items():
          if k != '%PLACEHOLDER%':
              filteredpaths[i]=k
      return filteredpaths
    
def getVersion():
    c = getConfig()
    if (env.getVersion() is False
        and (c["application"]["version"] is None
        or len(c["application"]["version"]) == 0)):
        raise Exception("Something is wrong with configuration file")
    elif (env.getVersion() is False):
        print("Version is set to " + c["application"]["version"])
        return c["application"]["version"]
    else:
        print("Version is set to " + env.getVersion())
        return env.getVersion()
    
def getLicense():
    c = getConfig()
    if (env.getLicense() is False
        and (c["application"]["license"] is None
        or len(c["application"]["license"]) == 0)):
        raise Exception("Something is wrong with configuration file")
    elif (env.getLicense() is False):
        print("License is set to " + c["application"]["license"])
        return c["application"]["license"]
    else:
        print("License is set to " + env.getLicense())
        return env.getLicense()

def getAuthor():
    c = getConfig()
    if (env.getAuthor() is False
        and (c["application"]["author"] is None
        or len(c["application"]["author"]) == 0)):
        raise Exception("Something is wrong with configuration file")
    elif (env.getAuthor() is False):
        print("Author is set to " + c["application"]["author"])
        return c["application"]["author"]
    else:
        print("Author is set to " + env.getAuthor())
        return env.getAuthor()