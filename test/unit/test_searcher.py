import unittest
from analyser import searcher

class TestTypes(unittest.TestCase):

    def testCheckType(self):

        a = type(searcher._validatePathConfig())
        self.assertIsInstance(searcher._validatePathConfig(), bool, "Wrong type")

if __name__ == "__main__":
    unittest.main()