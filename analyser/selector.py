import sys

from analyser import searcher as sch
from config import config
from config import logit

logger = logit.getLogger()

# do not replace print with logger
# they are for commnicating with the user

def getUsersChoice():
    if len(sys.argv) > 1:
        selectedOption = sys.argv[1]
    else:
        selectedOption = input("Hey, what would you like me to do?\n\
        To print info about the application, please, type '1'\n\
        To search filesystem for files, please, type '2'\n\
        To read existing archive files, please, type '3'\n\
        To print info about configured paths and extensions, type '4'\n")

    _switchOption(selectedOption)

def _switchOption(selectedOption):
    if selectedOption == '1':
        print("\nPrinting info about the application: \n")
        config.getVersion()
        config.getLicense()
        config.getAuthor()
        print("\n")

    elif selectedOption == '2':
        path = input("\nPlease enter a path to search\n")
        sch.searchCustomPath(path)
        print("\n")

    elif selectedOption == '3':
        print("\nSearching pre-configured paths\n")
        sch.searchConfiguredPaths()
        print("\n")

    elif selectedOption == '4':
        print("\n")
        print("--------------------")
        print("\nPrinting application search configuration data: \n")
        print("-- preconfigured paths to search:")
        print("\n" + str(config.getPathsConfigPrettyPrint()) + "\n")
        print("-- file extensions for filterting:")
        print("\n" + str(config.getConfiguredExtensionsPrettyPrint()) + "\n")
        print("--------------------")
        print("\n")

    else:
        print("\nSorry, you have made a wrong choice: please \n\
        pick '1' for app config data, \n\
        pick '2' for searching, \n\
        pick '3' for reading existing configuration. \n\
        pick '4' for app search config\n\
        Thank you!\n")
        print("\n")
