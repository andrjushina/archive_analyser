import os

from config import config
from config import logit
from . import archiver
from . import mediator

logger = logit.getLogger()

def searchCustomPath(path):
    if path is None:
        raise Exception("The entered path is an empty value")

    result = _validatePathString(path)
    
    if result == True:
        paths = {'source': path}
        processed_files = _processFilesForSelectedPaths(paths)
        logger.debug("Processed files are: " + str(processed_files))
        archiver.printToArchive(processed_files)
        if archiver.checkArchiveCreated() == True:
            print("Your archive files have been updated\n\
                   Please check your archive folder at:\n\
                   " + str(config.getArchiveFilePath()))
            return True
    else:
        print("\nYou have typed '" + path + "'\n")
        print("This path is either invalid or does not exist.\n")
        print("Please note that we only process absolute paths\n")
        print("Please check the syntax and try again.\n")

def searchConfiguredPaths():
    logger.debug("Paths config: " + str(config.getPathsConfig()))
    logger.debug("Extensions config: " + str(config.getConfiguredExtensions()))
 
    result = _validatePathConfig()

    if result == True:
        configured_paths = config.getPathsConfig()
        logger.debug("Configured paths are: " + str(configured_paths))
        processed_files = _processFilesForSelectedPaths(configured_paths)
        logger.debug("Processed files are: " + str(processed_files))
        archiver.printToArchive(processed_files)
        if archiver.checkArchiveCreated() == True:
            print("Your archive files have been updated\n\
                   Please check your archive folder at:\n\
                   " + str(config.getArchiveFilePath()))
            return True
    else:
        print("\nSorry, something is wrong with configuration of this tool.\n")
        print("Please use option 2 as a workaround.\n")
        return False

def _validatePathString(path):
    if os.path.isdir(path) == True and os.path.isabs(path) == True:
        return True
    elif os.path.isdir(path) == False:
        return False
    else:
        raise Exception("Bad path string")

def _validatePathConfig():
    if type(config.getPathsConfig()) == dict:
        return True
    elif type(config.getPathsConfig()) != dict:
        return False
    else:
        raise Exception("Bad path config")

def _processFilesForSelectedPaths(configured_paths):
    counter=0
    processed_files = ["id;file_name;file_path;file_metadata;meta_title;meta_artist;meta_genre;meta_year"]
    for cp in configured_paths.values():
        print("Processing configured path: " + cp)
        if _validatePathString(cp) == True:
            for root, dirs, files in os.walk(cp, topdown=False):
                for name in files:
                    if (_getFileExtension(name) in config.getIncludedExtensions() 
                        and _getFileExtension(name) in config.getTinyTagExtensions()):
                        counter +=1
                        abs_path = os.path.join(root, name)
                        md = mediator.getFileMetadataWithTinyTag(abs_path)
                        processed_files.append(str(counter) 
                                               + ";" 
                                               + name 
                                               + ";"
                                               +root
                                               + ";"
                                               + str(md["title"])
                                               +";"
                                               + str(md["artist"])
                                               +";"
                                               + str(md["genre"])
                                               +";"
                                               + str(md["year"])
                                               )
                    elif (_getFileExtension(name) in config.getIncludedExtensions() 
                        and _getFileExtension(name) not in config.getTinyTagExtensions()): 
                        counter +=1
                        abs_path = os.path.join(root, name)
                        md = mediator.getFileMetadataWithTinyTag(abs_path)
                        processed_files.append(str(counter) 
                                               + ";" 
                                               + name 
                                               + ";"
                                               +root
                                               + ";"
                                               + "n/a"
                                               +";"
                                               + "n/a"
                                               +";"
                                               + "n/a"
                                               +";"
                                               + "n/a"
                                               )
        elif _validatePathString(cp) == False:
            print("Something is wrong with configuration of paths to search\n\
                   Please check ./config/config.json for errors")
        else:
            raise Exception("Something went wrong while processing configuration files")

    return processed_files

def _getFileExtension(file_name):
    file_tuple = os.path.splitext(file_name)
    return file_tuple[1].lower()