import os.path
from os.path import join, dirname, exists, getsize
from config import config


def printToArchive(processed_files):
    if len(processed_files) < 1:
        raise Exception("The list of files is empty")
    if config.getArchiveMode() == 'rewrite':
        print("Rewriting the archive file")
        with open(config.getArchiveFilePath(), 'w') as file:
            for line in processed_files:
                file.write(line + '\n')
    else:
        print("Appending the archive file")
        with open(config.getArchiveFilePath(), 'a') as file:
            for line in processed_files:
                file.write(line + '\n')

def checkArchiveCreated():
    if (os.path.exists(config.getArchiveFilePath()) == True 
        and os.path.getsize(config.getArchiveFilePath()) > 1):
        return True
    else:
        return False