from tinytag import TinyTag 
from pprint import pprint
from config import logit

logger = logit.getLogger()

def getFileMetadataWithTinyTag(file):
    metadict = {}
    try:
        f = TinyTag.get(file)
        metadict = {"title": str(f.title), 
            "artist": str(f.artist), 
            "genre": str(f.genre), 
            "year": str(f.year), 
            "bitrate": str(f.bitrate), 
            "composer": str(f.composer), 
            "filesize": str(f.filesize), 
            "albumartist": str(f.albumartist), 
            "duration": str(f.duration), 
            "tracktotal": str(f.track_total)}

        logger.debug("Metainformation about " 
           + str(file) 
           + " is:\n" 
           + str(metadict))
        
        return metadict

    except:
        logger.debug("Sorry, file " + str(file) + " format is not currently supported by TinyTag library, metadata will be filled with 'n/a'")